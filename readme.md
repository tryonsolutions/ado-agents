# Repository for Azure DevOps Agents artifacts
This repository is used to house a custom script (and any other artifacts moving forward) that will be invoked to prepare new Azure DevOps nodes as they come online in the Azure Scale Set. We are using Azure Scale Sets to automatically provision and deprovision virtual machines and automatically check them in as Azure DevOps agents. This allows us to have a true CI/CD mindset with our build agents, and always be able to run our pipelines against fresh machines. 

## Configuration
As of right now, one machine will always be on and idle for a build to be ran on. But if more than one build is running, it will scale the machines up automatically. Once the agents (if there is more than 1) are idle for 15 minutes, it will power them down and delete them!

## Workflow
The workflow for the Azure Scale Set is as follows:

- **1.** As a build is requested, it spins up a new virtual machine inside of the Azure Scale Set
- **2.** After the virtual machine comes online, it processes the shell script to prepare it for our pipeline using the CustomScript extension.
- **3.** Once the CustomScript extensions runs successfully, it uses the Azure DevOps extension to automatically register/checkin with our Azure DevOps Agent Pool and then come online as an agent.
- **4.** The pipeline is executed on the virtual machine
- **5.** Once the pipeline finishes, the agent will stay online and idle for up to 15 minutes (incase you need to re-run or troubleshoot), and then it will go offline and be deleted.

## Shell Script
prep-ado-aget.sh: this is just a simple Shell script that installs some programs that our build needs to successfully run. Everytime the Scale Set builds a new virtual machine, the first step it does is to execute this script on it, and then check it into Azure DevOps so that it can be on standby to run builds.
